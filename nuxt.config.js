const pkg = require("./package");
const axios = require("axios");
testFolder = "./static/images/";
const fs = require("fs");
const images = [];

fs.readdir(testFolder, (err, files) => {
  files.forEach(file => {
    images.push(`images/${file}`);
  });
});

module.exports = {
  mode: "universal",
  env: {
    images
  },
  sitemap: {
    path: "/sitemap.xml",
    generate: true,
    hostname: "https://gregbenner1.gitlab.io/danikuzzi/"
  },
  /*
  ** Headers of the page
  */

  // https://gethead.info/#recommended-minimum
  head: {
    title: pkg.name,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  router: {
    base: "/danikuzzi/"
  },

  generate: {
    dir: 'public',
    routes: ["/about", "/portfolio", "/contact"]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: "#FFFFFF" },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    "@nuxtjs/axios",
    // Doc: https://buefy.github.io/#/documentation
    "nuxt-buefy",
    "@nuxtjs/sitemap"
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  http2: {
    push: true
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {}
  }
};
